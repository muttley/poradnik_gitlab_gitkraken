# Praca z gitem - repozytorium z assetami

## Spis treści

* [Potrzebne rzeczy](#potrzebne-rzeczy)
* [Rzeczy do wykonania jednorazowo - przygotowanie do pracy](#rzeczy-do-wykonania-jednorazowo-przygotowanie-do-pracy)
    * [Fork na GitLabie](#fork-na-gitlabie)
    * [Pierwsze uruchomienie GitKrakena](#pierwsze-uruchomienie-gitkrakena)
    * [Pobieranie repozytorium na własny komputer](#pobieranie-repozytorium-na-własny-komputer)
    * [Ustawianie poprawnego "remote"](#ustawianie-poprawnego-remote)
* [Rzeczy do powtarzania - podczas pracy z projektem](#rzeczy-do-powtarzania-podczas-pracy-z-projektem)
    * [Zanim rozpoczniesz pracę nad nowym assetem](#zanim-rozpoczniesz-pracę-nad-nowym-assetem)
    * [Tworzenie zmiany (praca nad assetami)](#tworzenie-zmiany-praca-nad-assetami)
    * [Wysyłanie zmian na serwer i tworzenie merge request](#wysyłanie-zmian-na-serwer-i-tworzenie-merge-request)

## Potrzebne rzeczy

- GitKraken - [https://www.gitkraken.com/download](https://www.gitkraken.com/download)
- konto w serwisie GitLab - [https://gitlab.com/users/sign_up](https://gitlab.com/users/sign_up)

## Rzeczy do wykonania jednorazowo - przygotowanie do pracy

### Fork na GitLabie

Wejdź na [https://gitlab.com/muttley/game_assets](https://gitlab.com/muttley/game_assets) i kliknij "Fork"

<img src="gitlab/fork/1.png" alt="drawing" width="70%"/>

Na następnej stronie wybieramy opcję `Public` a potem klikamy `Fork project`.

<img src="gitlab/fork/2.png" alt="drawing" width="70%"/>

Fork to takie coś, co pozwala Ci mieć swoje własne repozytorium, wprowadzać do niego poprawki, a następnie wysłać te poprawki do głównego repozytorium.

Po chwili zostajemy przeniesieni na stronę naszego projektu. Klikamy w przycisk `Clone`, a potem w przycisk kopiowania (nr 2 na obrazku). Zapisz gdzieś skopiowany link - przyda on się na etapie ściągania projektu.

<img src="gitlab/fork/3.png" alt="drawing" width="70%"/>

### Pierwsze uruchomienie GitKrakena

Uruchom GitKrakena i zaloguj się do niego. Możesz do tego wykorzystać swoje konto Google, GitHub lub założyć nowe konto (`Create a GitKraken Account`). 

<img src="gitkraken/first_run/1.png" alt="drawing" width="70%"/>

Na następnym ekranie podajemy swoje dane i klikamy `Create Profile`

<img src="gitkraken/first_run/2.png" alt="drawing" width="70%"/>

Na następnym ekranie wybieramy `Start with a Repo Tab`, a następnie zgadzamy się na warunki EULA.

<img src="gitkraken/first_run/3.png" alt="drawing" width="70%"/>

<img src="gitkraken/first_run/4.png" alt="drawing" width="70%"/>

### Pobieranie repozytorium na własny komputer

Kliknij w `Clone a repo`.

<img src="gitkraken/clone/1.png" alt="drawing" width="70%"/>

Wybierz `Clone (1)` -> `Clone with URL (2)`. Ustaw folder gdzie chcesz pobrać repozytorium `(3)`.

W polu `URL (4)` wpisz adres, który zapisałeś w pierwszym kroku - gdy robiłeś Fork. URL powinien być w formacie

```
https://gitlab.com/<twojnicknagitlabie>/game_assets.git
```

Następnie kliknij w `Clone the repo! (5)`

<img src="gitkraken/clone/2.png" alt="drawing" width="70%"/>

Jeśli wszystko zostało dobrze ustawione, to powinna pojawić się taka wiadomosć:

<img src="gitkraken/clone/3.png" alt="drawing" width="70%"/>

oraz taka, klikamy `Yes`

<img src="gitkraken/clone/4.png" alt="drawing" width="70%"/>

### Ustawianie poprawnego "remote"

Ten krok pozwoli nam poprawnie skonfigurować repozytorium, tak aby aktualizacje były pobierane z głównego repozytorium, natomiast tworzone zmiany były wysyłane na Twoje repozytorium - dzięki czemu później będzie można z nich stworzyć *Merge request*.

Po lewej stronie okna szukamy opcji chmury z napisem `REMOTE`, po najechaniu na to powinien się pojawić zielony plus. Klikamy w niego.

<img src="gitkraken/remote_config/1.png" alt="drawing" width="20%"/>

W następnym oknie ustawiamy nazwę `(1)`, adres pobierania `(2)` i adres wysyłania `(3)`.

<img src="gitkraken/remote_config/2.png" alt="drawing" width="50%"/>

Name `(1)`
```
root
```

Pull URL - adres głównego repozytorium `(2)`
```
https://gitlab.com/muttley/game_assets.git
```

Push URL - adres Twojego repozytorium (ten który kazałem skopiować i zapisać) `(3)`
```
https://gitlab.com/<twojnicknagitlabie>/game_assets.git
```

Gdy wszystko ustawiliśmy to możemy kliknąć `Add Remote (4)`.

Wracamy do głównego okna, po lewej stronie w zakładce `LOCAL` szukamy nazwy brancha `master` i klikamy na nią prawym przyciskiem, nastepnie z rozwijanego menu wybieramy `Set upstream`

<img src="gitkraken/remote_config/3.png" alt="drawing" width="50%"/>

W następnym panelu wybieramy `root` z rozwijanego menu `(1)`, a następnie klikamy `Submit (2)`

<img src="gitkraken/remote_config/4.png" alt="drawing" width="90%"/>

## Rzeczy do powtarzania - podczas pracy z projektem

### Zanim rozpoczniesz pracę nad nowym assetem

Przełącz się na branch master klikając w `branch (1)` i wybierając `master (2)` z listy. Następnie kliknij `Pull (3)` aby pobrać najnowsze aktualizacje z serwera - dzięki temu unikniesz konfliktów i problemów z wysyłaniem swoich zmian na serwer.

<img src="gitkraken/switch_and_pull/1.png" alt="drawing" width="90%"/>

### Tworzenie zmiany (praca nad assetami)

Załóżmy że Twoim zadaniem jest dodanie modelu i tekstury orka do folderu models.

Zanim przystąpisz do pracy trzeba stworzyć nowy branch, na którym będą znajdować się Twoje zmiany.

Wciśnij przycisk `Branch (1)`, a następnie wpisz nazwę brancha `(2)` - niech to będzie jakaś prosta nazwa, na przykład `add-orc` gdy dodajemy orka. Nazwa nie może się powtarzać - wymyśl nową nazwe jeśli kiedyś była używana już.

<img src="gitkraken/feature/1.png" alt="drawing" width="70%"/>

Teraz możemy zrobić faktyczną pracę - w folderu `models/` tworzymy folder `orc/` a następnie wrzucamy tam nasz model i teksturę.

<img src="gitkraken/feature/2.png" alt="drawing" width="50%"/>

Wracamy do GitKrakena - na górze listy zmian powinna nam się pojawić nienazwana zmiana podpisana jako `//WIP (1)`, klikamy w nią.

<img src="gitkraken/feature/3.png" alt="drawing" width="70%"/>

Po prawej stronie pojawi się panel w którym możemy dodać pliki do zmiany.

Po kolei mamy
- Lista zmiany (nie zostaną wysłane na serwer) - `(1) Unstaged Files`
- Lista zmian do opublikowania (one zostaną wysłane na serwer) - `(2) Staged Files`
- Opis zmiany - `(3) Commit Message`

Najeżdżamy na nasze modele i tekstury na liście i po kolei klikamy przycisk `Stage File (4)` żeby dodać je do listy zmian do opublikowania.

<img src="gitkraken/feature/4.png" alt="drawing" width="40%"/>

Lista `Staged Files` powinna zawierać nasze modele i tekstury, a `Commit Message` zawierać krótki opis tego co dodaliśmy lub zmieniliśmy. Gdy dodalismy już wszystko klikamy `Commit changes to files`.

<img src="gitkraken/feature/5.png" alt="drawing" width="40%"/>

Na liście zmian powinien pojawić się nasz commit (zmiana).

<img src="gitkraken/feature/6.png" alt="drawing" width="80%"/>

### Wysyłanie zmian na serwer i tworzenie merge request

W poprzednim kroku stworzyliśmy zmianę, teraz możemy wysłać ją na serwer aby później mogła trafić do głównego repozytorium. Klikamy w `Push (1)`.

<img src="gitkraken/push/1.png" alt="drawing" width="80%"/>

GitKraken zapyta nas gdzie ma wysłać te zmiany, wybieramy `root (1)` i nie zmieniamy nazwy brancha, po czym klikamy `Submit (2)`. Nastepnie GitKraken zapyta nas o nasze login i hasło do GitLaba, wpisujemy je i klikamy `Submit`. Dobrym pomysłem jest zaznaczyć `Remember Me` - dzięki temu GitKraken zapamięta nasze hasło.

<img src="gitkraken/push/2.png" alt="drawing" width="80%"/>

<img src="gitkraken/push/3.png" alt="drawing" width="80%"/>

Jeśli wszystko dobrze poszło to teraz możemy otworzyć przeglądarkę internetową i wejść na stronę repozytorium [https://gitlab.com/muttley/game_assets](https://gitlab.com/muttley/game_assets).

Powinniśmy zauważyć zielone pole informujące nas o tym, że wysłaliśmy właśnie brancha do swojego repozytorium. Kliknij `Create merge request` - dzięki temu będzie można dodać Twoje zmiany do głównego repozytorium.

<img src="gitkraken/push/4.png" alt="drawing" width="80%"/>

Na następnym ekranie znowu kliknij `Create merge request (1)`.

<img src="gitkraken/push/5.png" alt="drawing" width="80%"/>

Gratulacje, to Twój pierwszy merge request, teraz reszta zespołu przejrzy Twoje zmiany.

<img src="gitkraken/push/6.png" alt="drawing" width="80%"/>
